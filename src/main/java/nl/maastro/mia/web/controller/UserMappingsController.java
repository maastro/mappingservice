package nl.maastro.mia.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import nl.maastro.mia.web.entity.UserMappingEntity;
import nl.maastro.mia.web.model.MappingEntryModel;
import nl.maastro.mia.web.model.UpdateMappingModel;
import nl.maastro.mia.web.service.UserMappingService;

@Controller
@RequestMapping("/api/mapping")
public class UserMappingsController {

	private static final Logger LOGGER = Logger.getLogger(UserMappingsController.class);
	
	@Autowired
	private UserMappingService userMappingService;
	
	@RequestMapping(value = "/usermappings/{userId}", method = RequestMethod.GET)
	public String userMappings(@PathVariable("userId") String userId, Model model){
		
		List<MappingEntryModel> entryModels = new ArrayList<>();
		for (UserMappingEntity userMapping : userMappingService.getMappingsByUser(userId)){
			MappingEntryModel entryModel = new MappingEntryModel();
			entryModel.setRtog(userMapping.getRtog());
			entryModel.setRoi(userMapping.getRoi());
			entryModel.setDelete(0);
			entryModel.setUserId(userId);
			
			entryModels.add(entryModel);
		}
		
		UpdateMappingModel userMapping = new UpdateMappingModel();
		userMapping.setMappingEntryModelList(entryModels);
		
		model.addAttribute("userMapping", userMapping);
		
		return "usermappings";
	}
	
	@RequestMapping(value = "/deleteusermapping", method = RequestMethod.POST)
	public String deleteUserMapping(@ModelAttribute("userMapping") UpdateMappingModel userMapping, BindingResult result, Model model){
		
		if (userMapping == null || userMapping.getMappingEntryModelList() == null || userMapping.getMappingEntryModelList().isEmpty()){
			LOGGER.warn("No mappings to delete");
			return this.userMappingsAdapter(model);
		}
		
		for (MappingEntryModel mappingModel : userMapping.getMappingEntryModelList()){
			
			if (mappingModel != null && !StringUtils.isBlank(mappingModel.getRoi())
					&& !StringUtils.isBlank(mappingModel.getRtog())
					&& !StringUtils.isBlank(mappingModel.getUserId())
					&& mappingModel.getDelete() != 0){
				
				userMappingService.deleteMapping(userMappingService.getByRtogAndRoiAndUser(
						mappingModel.getRtog(), mappingModel.getRoi(), mappingModel.getUserId()));
				
				LOGGER.info("Deleted mapping: " + mappingModel.getRtog() + " <--> " + mappingModel.getRoi() 
				+ " for user: " + mappingModel.getUserId());
			}
		}
		
		return this.userMappingsAdapter(model);
	}
	
	
	@RequestMapping(value = "/usermappingsadapter", method = RequestMethod.GET)
	public String userMappingsAdapter(Model model){		
		return "usermappingsadapter";
	}
}
