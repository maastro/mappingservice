package nl.maastro.mia.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import nl.maastro.mia.web.dto.MappingDto;
import nl.maastro.mia.web.entity.UserMappingEntity;
import nl.maastro.mia.web.entity.enumeration.MappingLevelEnum;
import nl.maastro.mia.web.model.MappingEntryModel;
import nl.maastro.mia.web.model.UpdateMappingModel;
import nl.maastro.mia.web.service.MappingService;
import nl.maastro.mia.web.service.UserMappingService;

@Controller
@RequestMapping("/api/mapping")
public class ApproveMappingsController {

	
	private static final Logger LOGGER = Logger.getLogger(ApproveMappingsController.class);

	@Autowired
	private MappingService mappingService;
	
	@Autowired
	private UserMappingService userMappingService;
	
	
	@RequestMapping(value = "/approvemapping", method = RequestMethod.GET)
	public String approveMapping(Model model){
		
		List<MappingDto> suggestedMappings = userMappingService.getDistinctSuggestedMappings();
		
		UpdateMappingModel approveMappingModel = new UpdateMappingModel();
		List<MappingEntryModel> entryModels = new ArrayList<>();
		
		for (MappingDto mappingDto : suggestedMappings){
			MappingEntryModel entryModel = new MappingEntryModel();
			entryModel.setApproveLevel(0);
			entryModel.setRtog(mappingDto.getRtog());
			entryModel.setRoi(mappingDto.getRoi());
			
			entryModels.add(entryModel);
			
		}
		approveMappingModel.setMappingEntryModelList(entryModels);
		model.addAttribute("approveMappingModel", approveMappingModel);
		
		return "approvemapping";
	}
	
	@RequestMapping(value = "/updatemappings", method = RequestMethod.POST)
	public String updateMappings(@ModelAttribute UpdateMappingModel approveMappingModel, BindingResult result, Model model){
		List<MappingEntryModel> failedEntryModels = new ArrayList<>();
		
		for (MappingEntryModel entryModel : approveMappingModel.getMappingEntryModelList()){
			switch(entryModel.getApproveLevel()){
			case 1://DISMISS
				this.dismissMapping(entryModel);
				break;
			case 2://APPROVE
				if (!this.approveMapping(entryModel)){
					failedEntryModels.add(entryModel);
				}
				break;
			default://DO NOTHING
				break;
			}
		}
		
		if (!failedEntryModels.isEmpty()){
			String errorMessage = "Failed to insert to RMO: ";
			for (MappingEntryModel entryModel : failedEntryModels){
				errorMessage += "{" + entryModel.getRtog() + "=" + entryModel.getRoi() + "}";
			}
			model.addAttribute("error", errorMessage);
		}
		return this.approveMapping(model);
	}
	
	private void dismissMapping(MappingEntryModel entryModel){
		LOGGER.info("Set mapping to STORE: " + entryModel.getRtog() + "=" + entryModel.getRoi());
		
		UserMappingEntity userMapping = userMappingService.getByRtogAndRoiAndMappingLevel(
				entryModel.getRtog(), entryModel.getRoi(), MappingLevelEnum.SUGGEST);
		
		userMapping.setMappingLevel(MappingLevelEnum.STORE);
		userMappingService.insertUserMapping(userMapping);
	}
	
	private boolean approveMapping(MappingEntryModel entryModel){
		
		LOGGER.info("Adding new Mapping to Global: " + entryModel.getRtog() + "=" + entryModel.getRoi());
		boolean inserted = mappingService.insertMapping(entryModel.getRtog(), entryModel.getRoi());
		
		if (inserted){
			//Set to store, same as with dismiss:
			this.dismissMapping(entryModel);
		}
		return inserted;
	}
}
