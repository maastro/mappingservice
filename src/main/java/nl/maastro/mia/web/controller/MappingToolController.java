package nl.maastro.mia.web.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import nl.maastro.mia.web.entity.UserMappingEntity;
import nl.maastro.mia.web.entity.enumeration.MappingLevelEnum;
import nl.maastro.mia.web.model.MappingEntryModel;
import nl.maastro.mia.web.model.MappingModel;
import nl.maastro.mia.web.service.MappingService;
import nl.maastro.mia.web.service.ReportService;
import nl.maastro.mia.web.service.UserMappingService;

@Controller
@RequestMapping("/api/mapping")
public class MappingToolController {

	private static final Logger LOGGER = Logger.getLogger(MappingToolController.class);
	private static final String[] ROLES = new String[]{"ROLE_USER", "ROLE_ADMIN"};

	@Autowired
	private MappingService mappingService;

	@Autowired
	private ReportService reportService;

	@Autowired
	private UserMappingService userMappingService;

	private static final String NOT_PRESENT = "NotPresent";


	@RequestMapping(value = "/mappingtool", method = RequestMethod.GET)
	public String mappingTool(@RequestParam(required = false) List<String> rois, @RequestParam(required = false) List<String> rtogs, 
			@RequestParam(required = false) String userId, @RequestParam(required = false) String userRole, 
			@RequestParam(required = false) String containerId, @RequestParam(required = false) String managerName, Model model){

		if (rois == null || rois.isEmpty()){
			model.addAttribute("error", "ROI list is null or empty.");
			LOGGER.warn("ROI list is null or empty.");
			return "error";
		}

		if (rtogs == null || rtogs.isEmpty()){
			model.addAttribute("error", "RTOG list is null or empty.");
			LOGGER.warn("RTOG list is null or empty.");
			return "error";
		}	

		if (StringUtils.isBlank(userId)){
			model.addAttribute("error", "UserId is null or empty.");
			LOGGER.warn("UserId is null or empty.");
			return "error";
		}

		if (StringUtils.isBlank(containerId)){
			model.addAttribute("error", "ContainerId is null or empty.");
			LOGGER.warn("ContainerId is null or empty.");
			return "error";
		}

		if (StringUtils.isBlank(userRole)){
			model.addAttribute("error", "UserRole is null or empty.");
			LOGGER.warn("UserRole is null or empty.");
			return "error";
		}
		else if (!Arrays.asList(ROLES).contains(userRole)){
			model.addAttribute("error", userRole + " is not a proper role.");
			LOGGER.warn(userRole + " is not a proper role.");
			return "error";
		}

		if (StringUtils.isBlank(managerName)){
			model.addAttribute("error", "ManagerName is null or empty.");
			LOGGER.warn("ManagerName is null or empty.");
			return "error";
		}

		//Preprocess existing mappings
		List<MappingEntryModel> preMappedList = new ArrayList<>();
		List<String> nonIgnoredRois = mappingService.getNonIgnoredRoiCandidates(rois);

		Map<String, String> mappings = mappingService.getMappings(
				nonIgnoredRois, 
				rtogs,
				userId);
		List<String> roiCandidates = mappingService.getRoiCandidates(mappings, nonIgnoredRois);
		Map<String, String> userMappings = 
				this.userMappingEntityToMapping(
						userMappingService.getMappingsByUser(userId),
						rtogs,
						roiCandidates);
		mappings.putAll(userMappings);

		List<String> rtogsCopy = new ArrayList<>(rtogs);
		for (String rtog : rtogsCopy){
			if (mappings.containsKey(rtog) && !StringUtils.isBlank(mappings.get(rtog))){

				MappingEntryModel entryModel = new MappingEntryModel();
				entryModel.setRtog(rtog);
				entryModel.setRoi(mappings.get(rtog));
				preMappedList.add(entryModel);

				rtogs.remove(rtog);
				roiCandidates.remove(mappings.get(rtog));
			}
		}

		model.addAttribute("rois", roiCandidates);
		model.addAttribute("rtogs", rtogs);
		model.addAttribute("containerId", containerId);
		model.addAttribute("userId", userId);
		model.addAttribute("userRole", userRole);
		model.addAttribute("managerName", managerName);
		model.addAttribute("preMappedList", preMappedList);
		model.addAttribute("mapping", true);
		model.addAttribute(NOT_PRESENT, NOT_PRESENT);
		model.addAttribute("mappingModel", this.initialMappingModel(rtogs));

		return "mappingtool";
	}

	private MappingModel initialMappingModel(List<String> rtogs){
		MappingModel mappingModel = new MappingModel();

		for (String rtog : rtogs){
			MappingEntryModel entry = new MappingEntryModel();

			entry.setRtog(rtog);
			entry.setMappingLevel(0);

			mappingModel.getMappingList().add(entry);
		}

		return mappingModel;
	}


	@RequestMapping(value = "/validatemapping", method = RequestMethod.POST)
	public String validateInput(@ModelAttribute("mappingModel") MappingModel mappingModel, BindingResult result, Model model){

		if (result.hasErrors()){
			model.addAttribute("error", result.getAllErrors().get(0));
			LOGGER.error("BindingResult error on /validatemapping: " + result.getAllErrors().toString());
		}
		else if (this.hasDoubleRoiMappings(mappingModel)){
			model.addAttribute("error", "Cannot map an ROI to multiple different RTOGs");
			LOGGER.warn("Cannot map an ROI to multiple different RTOGs");
		}
		else {
			LOGGER.info("Storing results in user's mapping database.");
			for (UserMappingEntity entity : this.getUserMappingEntities(mappingModel)){
				userMappingService.insertUserMapping(entity);
			}

			try {
				LOGGER.info("Reporting finished mappings back to manager");
				reportService.reportToManager(mappingModel.getContainerId(), mappingModel.getUserId(), 
						mappingModel.getManagerName(), this.getAllMappings(mappingModel));
			} catch(Exception e){
				LOGGER.error("Failed to contact manager: ", e);
				model.addAttribute("error", "Failed to contact manager.");
				return "error";
			}

			return "mapped";
		}

		model.addAttribute("rois", mappingModel.getRoiList());

		List<String> rtogs = new ArrayList<>();
		for (MappingEntryModel entry : mappingModel.getMappingList()){
			rtogs.add(entry.getRtog());
		}
		model.addAttribute("rtogs", rtogs);
		model.addAttribute("preMappedList", mappingModel.getPreMappedList());
		model.addAttribute("containerId", mappingModel.getContainerId());
		model.addAttribute("userId", mappingModel.getUserId());
		model.addAttribute("managerName", mappingModel.getManagerName());
		model.addAttribute("NotPresent", NOT_PRESENT);
		model.addAttribute("mapping", true);

		return "mappingtool";
	}

	private Map<String, String> getAllMappings(MappingModel mappingModel){
		Map<String, String> mappings = new HashMap<>();

		mappingModel.getMappingList().addAll(mappingModel.getPreMappedList());
		for (MappingEntryModel entry : mappingModel.getMappingList()){
			mappings.put(entry.getRtog(), entry.getRoi());
		}

		return mappings;
	}

	private boolean hasDoubleRoiMappings(MappingModel mappingModel){
		Map<String, Integer> roiCountMap = new HashMap<>();

		for (String roi : mappingModel.getRoiList()){
			roiCountMap.put(roi, 0);
		}

		for (MappingEntryModel entry : mappingModel.getMappingList()){
			String roi = entry.getRoi();
			if (!NOT_PRESENT.equals(roi)){
				roiCountMap.put(roi, roiCountMap.get(roi) + 1);
			}
		}

		for (Iterator<String> it = roiCountMap.keySet().iterator(); it.hasNext();){
			if (roiCountMap.get(it.next()) > 1){
				return true;
			}
		}

		return false;
	}

	private List<UserMappingEntity> getUserMappingEntities(MappingModel mappingModel){
		List<UserMappingEntity> entities = new ArrayList<>();

		for (MappingEntryModel entry : mappingModel.getMappingList()){
			UserMappingEntity entity = new UserMappingEntity();
			entity.setRtog(entry.getRtog());
			entity.setRoi(entry.getRoi());
			entity.setUserId(mappingModel.getUserId());

			switch (entry.getMappingLevel()){
			case 1: 
				entity.setMappingLevel(MappingLevelEnum.STORE);
				entities.add(entity);
				break;
			case 2: 
				entity.setMappingLevel(MappingLevelEnum.SUGGEST);
				entities.add(entity);
				break;
			default: 
				break;
			}
		}
		return entities;
	}


	private Map<String, String> userMappingEntityToMapping(List<UserMappingEntity> mappingEntities, 
			List<String> rtogs, List<String> rois){

		Map<String, String> userMappings = new HashMap<>();

		for (UserMappingEntity mappingEntity : mappingEntities){
			if (rtogs.contains(mappingEntity.getRtog()) && rois.contains(mappingEntity.getRoi())){
				userMappings.put(mappingEntity.getRtog(), mappingEntity.getRoi());
			}
		}

		return userMappings;
	}
}