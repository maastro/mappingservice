package nl.maastro.mia.web.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

@RestController
@RequestMapping("/api")
public class RedirectController {

	private static final Logger LOGGER = Logger.getLogger(RedirectController.class);
	private static final String CONTAINER_URL_SUFFIX = "/#/container";
	
	@Autowired
	private EurekaClient eurekaClient;
	
	@Value("${micro.gui:mia}")
	private String miaGuiName;
	
	
	@RequestMapping(value = "/forwardcontainerview", method = RequestMethod.GET)
	public ModelAndView returnToContainerView(){
		String miaGuiHomeUrl = this.getMiaGuiBaseUrl();
		
		if (StringUtils.isBlank(miaGuiHomeUrl)){
			LOGGER.warn("Unable to retrieve miaGui homepage from eureka client. Cannot redirect.");
			return this.failedToRedirect();
		}
		
		UriComponentsBuilder builder = ServletUriComponentsBuilder.fromUriString(miaGuiHomeUrl + CONTAINER_URL_SUFFIX);
		String url = builder.build().encode().toUriString();
		
		return new ModelAndView("redirect:" + url);
	}
	
	private ModelAndView failedToRedirect(){
		ModelAndView modelAndView = new ModelAndView("error");
		modelAndView.addObject("error", "Failed to redirect to container overview.");
		return modelAndView;
	}
	
	
	private String getMiaGuiBaseUrl(){
		Application app = eurekaClient.getApplication(miaGuiName);
		if (app == null){
			return null;
		}
		if (app.getInstances().isEmpty()){
			return null;
		}
		return app.getInstances().get(0).getHomePageUrl();
	}
}
