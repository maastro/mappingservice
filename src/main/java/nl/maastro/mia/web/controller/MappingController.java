package nl.maastro.mia.web.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.web.dto.RoiRtogListDto;
import nl.maastro.mia.web.service.MappingService;

@RestController
@RequestMapping("/api/mapping")
public class MappingController {

	@Autowired
	private MappingService mappingService;
	
	@RequestMapping(value = "/getmappings", method = RequestMethod.POST)
	public Map<String, String> getMappings(@RequestBody RoiRtogListDto roiRtogListDto){
		
		if (roiRtogListDto == null 
				|| roiRtogListDto.getRoiList() == null || roiRtogListDto.getRoiList().isEmpty()
				|| roiRtogListDto.getRtogList() == null || roiRtogListDto.getRtogList().isEmpty()){
			return Collections.emptyMap();
		}
		List<String> roiCandidates = mappingService.getNonIgnoredRoiCandidates(roiRtogListDto.getRoiList());
		return mappingService.getMappings(roiCandidates, roiRtogListDto.getRtogList(), roiRtogListDto.getUserId());
	}
}
