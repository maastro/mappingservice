package nl.maastro.mia.web.model;

import java.util.ArrayList;
import java.util.List;

public class UpdateMappingModel {

	private List<MappingEntryModel> mappingEntryModelList = new ArrayList<>();
	

	public List<MappingEntryModel> getMappingEntryModelList() {
		return mappingEntryModelList;
	}

	public void setMappingEntryModelList(List<MappingEntryModel> mappingEntryModelList) {
		this.mappingEntryModelList = mappingEntryModelList;
	}
}
