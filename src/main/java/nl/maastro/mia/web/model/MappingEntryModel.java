package nl.maastro.mia.web.model;

public class MappingEntryModel {

	private String rtog;
	private String roi;
	private int mappingLevel;
	private int approveLevel;
	private String userId;
	private int delete;
	
	
	public String getRtog() {
		return rtog;
	}
	public void setRtog(String rtog) {
		this.rtog = rtog;
	}
	public String getRoi() {
		return roi;
	}
	public void setRoi(String roi) {
		this.roi = roi;
	}
	public int getMappingLevel() {
		return mappingLevel;
	}
	public void setMappingLevel(int mappingLevel) {
		this.mappingLevel = mappingLevel;
	}
	public int getApproveLevel() {
		return approveLevel;
	}
	public void setApproveLevel(int approveLevel) {
		this.approveLevel = approveLevel;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getDelete() {
		return delete;
	}
	public void setDelete(int delete) {
		this.delete = delete;
	}
}
