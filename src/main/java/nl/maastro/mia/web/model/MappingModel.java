package nl.maastro.mia.web.model;

import java.util.ArrayList;
import java.util.List;

public class MappingModel {

	private List<MappingEntryModel> mappingList = new ArrayList<>();
	private List<MappingEntryModel> preMappedList = new ArrayList<>();
	private List<String> roiList = new ArrayList<>();
	
	private String userId;
	private String containerId;
	private String managerName;

	public List<MappingEntryModel> getMappingList() {
		return mappingList;
	}

	public void setMappingList(List<MappingEntryModel> mappingList) {
		this.mappingList = mappingList;
	}

	public List<String> getRoiList() {
		return roiList;
	}

	public void setRoiList(List<String> roiList) {
		this.roiList = roiList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public List<MappingEntryModel> getPreMappedList() {
		return preMappedList;
	}

	public void setPreMappedList(List<MappingEntryModel> preMappedList) {
		this.preMappedList = preMappedList;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

}
