package nl.maastro.mia.web.dto;

import org.apache.commons.lang.builder.HashCodeBuilder;

public class MappingDto {

	private String rtog;
	private String roi;
	
	public MappingDto(){ }
	
	public MappingDto(String rtog, String roi){
		this.rtog = rtog;
		this.roi = roi;
	}
	
	public String getRtog() {
		return rtog;
	}
	public void setRtog(String rtog) {
		this.rtog = rtog;
	}
	public String getRoi() {
		return roi;
	}
	public void setRoi(String roi) {
		this.roi = roi;
	}
	
	@Override
    public boolean equals(Object object){
		if (!(object instanceof MappingDto)){
			return false;
		}
		
		return this.rtog.equals(((MappingDto) object).getRtog()) && this.roi.equals(((MappingDto) object).getRoi()); 
	}
	
	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            append(this.rtog).
            append(this.roi).
            toHashCode();
    }

}
