package nl.maastro.mia.web.dto;

import java.util.List;

public class RoiRtogListDto {

	private List<String> roiList;
	private List<String> rtogList;
	private String userId;
	
	
	public List<String> getRoiList() {
		return roiList;
	}
	public void setRoiList(List<String> roiList) {
		this.roiList = roiList;
	}
	public List<String> getRtogList() {
		return rtogList;
	}
	public void setRtogList(List<String> rtogList) {
		this.rtogList = rtogList;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
