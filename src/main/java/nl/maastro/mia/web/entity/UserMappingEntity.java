package nl.maastro.mia.web.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import nl.maastro.mia.web.entity.enumeration.MappingLevelEnum;

@Entity
public class UserMappingEntity {

	@Id
	@GeneratedValue
	private long id;
	private String userId;
	private String roi;
	private String rtog;
	private MappingLevelEnum mappingLevel;
	
	
	public String getRoi() {
		return roi;
	}
	public void setRoi(String roi) {
		this.roi = roi;
	}
	public String getRtog() {
		return rtog;
	}
	public void setRtog(String rtog) {
		this.rtog = rtog;
	}
	public MappingLevelEnum getMappingLevel() {
		return mappingLevel;
	}
	public void setMappingLevel(MappingLevelEnum mappingLevel) {
		this.mappingLevel = mappingLevel;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString(){
		return "UserMappingEntity:{" +
				"rtog=" + this.rtog +
				",roi=" + this.roi +
				",user=" + this.userId +
				",level=" + this.mappingLevel +
				"}";
	}
}
