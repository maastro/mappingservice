package nl.maastro.mia.web.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class GlobalMappingEntity {

	@Id
	@GeneratedValue
	private long id;
	
	private String roi;
	private String rtog;
	
	
	public String getRoi() {
		return roi;
	}
	public void setRoi(String roi) {
		this.roi = roi;
	}
	public String getRtog() {
		return rtog;
	}
	public void setRtog(String rtog) {
		this.rtog = rtog;
	}
}
