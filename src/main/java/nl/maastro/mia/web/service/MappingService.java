package nl.maastro.mia.web.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import nl.maastro.mia.web.entity.GlobalMappingEntity;
import nl.maastro.mia.web.entity.UserMappingEntity;
import nl.maastro.mia.web.repository.GlobalMappingRepository;
import nl.maastro.mia.web.repository.UserMappingRepository;

@Service
@Configuration
@ConfigurationProperties(prefix = "mappings")
public class MappingService {
	@Autowired
	UserMappingRepository userMappingRepository;

	@Autowired
	GlobalMappingRepository globalMappingRepository;
	private ArrayList<String> ignoreList;

	private static final Logger LOGGER = Logger.getLogger(MappingService.class);

	public Map<String, String> getMappings(List<String> roiList, List<String> rtogList, String userId){
		LOGGER.info("Getting mappings for user " + userId);

		Map<String, String> mappings = new HashMap<>();

		for(String rtog : rtogList){
			mappings.put(rtog, "");
		}

		setAlreadyCorrectMappings(mappings, roiList);
		setGlobalMappings(mappings, roiList);
		setUserMappings(mappings, roiList, userId);
		setNotPresentMappings(mappings, getRoiCandidates(mappings, roiList));

		LOGGER.info("Mappings for user " + userId + ": " + mappings);
		return mappings;
	}

	private void setUserMappings(Map<String, String> mappings, List<String> roiList, String userId) {
		if(userId == null || userId.isEmpty()){
			LOGGER.warn("Illegal input for userMapping, user: " + userId);
			return;
		}

		for(String roi : roiList){
			UserMappingEntity userMapping = userMappingRepository.findOneByRoiAndUserId(roi, userId);
			if (userMapping != null){
				String rtog = userMapping.getRtog();
				if(mappings.keySet().contains(rtog)){
					mappings.put(rtog, roi);
					LOGGER.info("UserMapping found for rtog: " + rtog + " roi: " + roi + " userId: " + userId);
				}
			}
		}
	}

	private void setGlobalMappings(Map<String, String> mappings, List<String> roiList) {
		for(String roi : roiList){
			GlobalMappingEntity globalMapping = globalMappingRepository.findOneByRoi(roi);
			if (globalMapping != null){
				String rtog = globalMapping.getRtog();
				if(mappings.keySet().contains(rtog)){
					mappings.put(rtog, roi);
					LOGGER.info("GlobalMapping found for rtog: " + rtog + " roi: " + roi);
				}
			}
		}
	}

	private void setNotPresentMappings(Map<String, String> mappings, List<String> roiCandidates) {
		for(Entry<String, String> entry : mappings.entrySet()){
			if(roiCandidates.isEmpty() &&
					(entry.getValue().isEmpty() || entry.getValue() == null)){
				mappings.put(entry.getKey(), "NotPresent");
			}
		}
	}

	private void setAlreadyCorrectMappings(Map<String, String> mappings, List<String> roiList) {
		for(Entry<String, String> entry : mappings.entrySet()){
			if(roiList.contains(entry.getKey())){
				mappings.put(entry.getKey(), entry.getKey());
			}
		}
	}

	public boolean insertMapping(String rtog, String roi){
		GlobalMappingEntity globalMapping = new GlobalMappingEntity();
		globalMapping.setRoi(roi);
		globalMapping.setRtog(rtog);

		return globalMappingRepository.save(globalMapping) != null;
	}

	public List<String> getRoiCandidates(Map<String,String> mappings, List<String> roiList){
		List<String> roiCandidates = new ArrayList<>(roiList);
		for(String usedRoi : mappings.values()){
			roiCandidates.remove(usedRoi);
		}
		return roiCandidates;
	}

	public List<String> getNonIgnoredRoiCandidates(List<String> roiList){
		List<String> roiCandidates = roiList;
		removeCandidatesInIgnoreList(roiCandidates);
		return roiCandidates;
	}

	public void removeCandidatesInIgnoreList(List<String> roiCandidates) {
		for (Iterator<String> roiIterator = roiCandidates.iterator();
				roiIterator.hasNext();){
			String roiCandidate = roiIterator.next();
			if(isInIgnoreList(roiCandidate)){
				roiIterator.remove();
				continue;
			}
		}
	}

	public boolean isInIgnoreList(String roi) {
		for(String regex : ignoreList){
			if(roi.matches(regex)){
				return true;
			}
		}
		return false;
	}

	public ArrayList<String> getIgnoreList() {
		return ignoreList;
	}

	public void setIgnoreList(ArrayList<String> ignoreList) {
		this.ignoreList = ignoreList;
	}
}
