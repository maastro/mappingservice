package nl.maastro.mia.web.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import nl.maastro.mia.web.dto.MappingResultDto;

@Service
public class ReportService {

	@Autowired
	RestTemplate restTemplate;
	
	private static final Logger LOGGER = Logger.getLogger(ReportService.class);
	
	
	
	public void reportToManager(String containerId, String userId, String managerName, Map<String, String> mappings){
		MappingResultDto mappingResult = new MappingResultDto();
		
		mappingResult.setContainerId(containerId);
		mappingResult.setUserId(userId);
		mappingResult.setRtogRoiMap(mappings);
		
		String url = "http://" + managerName + "/api/map/container";
		
		if (restTemplate.postForEntity(url, mappingResult, ResponseEntity.class)
				.getStatusCode().is2xxSuccessful()){
			LOGGER.debug("Successfully posted mappings to manager");
		}
		else {
			LOGGER.warn("Failed to post mappings to manager");
		}
	}
}
