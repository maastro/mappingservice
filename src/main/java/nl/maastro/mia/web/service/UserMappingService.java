package nl.maastro.mia.web.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.web.dto.MappingDto;
import nl.maastro.mia.web.entity.UserMappingEntity;
import nl.maastro.mia.web.entity.enumeration.MappingLevelEnum;
import nl.maastro.mia.web.repository.UserMappingRepository;

@Service
public class UserMappingService {

	private static final Logger LOGGER = Logger.getLogger(UserMappingService.class);
	
	@Autowired
	private UserMappingRepository mappingRepository;
	
	public List<MappingDto> getDistinctSuggestedMappings(){
		List<UserMappingEntity> suggestedMappings = mappingRepository.findAllByMappingLevel(MappingLevelEnum.SUGGEST);
		List<MappingDto> suggestedMappingDtos = new ArrayList<>();
		
		for (UserMappingEntity entity : suggestedMappings) {
			MappingDto dto = new MappingDto(entity.getRtog(), entity.getRoi());
			if (!suggestedMappingDtos.contains(dto)){
				suggestedMappingDtos.add(dto);
			}
		}
		
		return suggestedMappingDtos;
	}
	
	public UserMappingEntity getByRtogAndRoiAndMappingLevel(String rtog, String roi, MappingLevelEnum mappingLevel){
		return mappingRepository.findTop1ByRtogAndRoiAndMappingLevel(rtog, roi, mappingLevel);
	}
	
	public UserMappingEntity getByRtogAndRoiAndUser(String rtog, String roi, String user){
		return mappingRepository.findTop1ByRtogAndRoiAndUserId(rtog, roi, user);
	}
	
	public void deleteMapping(UserMappingEntity entity){
		mappingRepository.delete(entity);
	}
	
	public List<UserMappingEntity> getMappingsByUser(String user){
		return mappingRepository.findAllByUserId(user);
	}
	
	public synchronized void insertUserMapping(UserMappingEntity entity){
		if (entity == null){
			LOGGER.error("Cannot add entity to database, because the entity is null");
			return;
		}
		if (entity.getMappingLevel() == null
				|| StringUtils.isBlank(entity.getRoi())
				|| StringUtils.isBlank(entity.getRtog())
				|| StringUtils.isBlank(entity.getUserId())){
			LOGGER.error("Cannot add entity to database, because a field is null: " + entity.toString());
			return;
		}
		if (mappingRepository.findOneByRoiAndUserId(entity.getRoi(), entity.getUserId()) != null){
			LOGGER.error("An entity already exists in the database for this roi");
			return;
		}

		mappingRepository.save(entity);
	}
	
}
