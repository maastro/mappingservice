package nl.maastro.mia.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.web.entity.UserMappingEntity;
import nl.maastro.mia.web.entity.enumeration.MappingLevelEnum;

@Repository
public interface UserMappingRepository extends JpaRepository<UserMappingEntity, Long>{

	List<UserMappingEntity> findAllByMappingLevel(MappingLevelEnum mappingLevel);
	List<UserMappingEntity> findAllByUserId(String userId);
	UserMappingEntity findTop1ByRtogAndRoiAndMappingLevel(String rtog, String roi, MappingLevelEnum mappingLevel);
	UserMappingEntity findTop1ByRtogAndRoiAndUserId(String rtog, String roi, String userId);
	UserMappingEntity findOneByRoiAndUserId(String roi, String userId);
}
