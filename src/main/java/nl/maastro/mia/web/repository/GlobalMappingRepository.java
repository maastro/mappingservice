package nl.maastro.mia.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.web.entity.GlobalMappingEntity;

@Repository
public interface GlobalMappingRepository extends JpaRepository<GlobalMappingEntity, Long>{

	GlobalMappingEntity findOneByRoi(String roi);
	
	GlobalMappingEntity findOneByRtog(String rtog);
}
