package nl.maastro.mia;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import nl.maastro.mia.web.dto.MappingDto;
import nl.maastro.mia.web.entity.UserMappingEntity;
import nl.maastro.mia.web.entity.enumeration.MappingLevelEnum;
import nl.maastro.mia.web.service.UserMappingService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MappingServiceApplication.class)
public class UserMappingRepositoryTests {

	@Autowired
	private UserMappingService userMappingService;

	private static final Object[][] INIT_VARIABLES = new Object[][]{
		{"GTV-1", "GTVp", "user1", MappingLevelEnum.STORE}, 
		{"GTV_2", "GTVn", "user1", MappingLevelEnum.STORE},
		{"Lung_1", "Lung_L", "user1", MappingLevelEnum.SUGGEST},
		{"GTV-1", "GTVp", "user2", MappingLevelEnum.STORE},
		{"Eso", "Esophoges", "user2", MappingLevelEnum.SUGGEST},
		{"Spl33n", "Spleen", "user2", MappingLevelEnum.STORE},
		{null, "Something", "user2", MappingLevelEnum.SUGGEST},
		{"Lung_1", "Lung_L", "user2", MappingLevelEnum.SUGGEST}};

		@Before
		public void initDatabase(){

			for (Object[] data : INIT_VARIABLES){

				UserMappingEntity e = new UserMappingEntity();
				e				.setRoi((String) data[0]);
				e				.setRtog((String) data[1]);
				e				.setUserId((String) data[2]);
				e				.setMappingLevel((MappingLevelEnum) data[3]);

				userMappingService.insertUserMapping(e);
			}
		}

		@Test
		public void distinctSuggestionsTest(){
			List<MappingDto> suggestedMappings = userMappingService.getDistinctSuggestedMappings();

			assertEquals(2, suggestedMappings.size());
			assertEquals("Lung_1", suggestedMappings.get(0).getRoi());
			assertEquals("Esophoges", suggestedMappings.get(1).getRtog());
		}

		@Test
		public void userMappingsTest(){
			List<UserMappingEntity> user1Mappings = userMappingService.getMappingsByUser("user1");

			assertEquals(3, user1Mappings.size());

			List<UserMappingEntity> user2Mappings = userMappingService.getMappingsByUser("user2");

			assertEquals(4, user2Mappings.size());
		}
}
