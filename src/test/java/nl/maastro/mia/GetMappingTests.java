package nl.maastro.mia;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import nl.maastro.mia.web.entity.GlobalMappingEntity;
import nl.maastro.mia.web.repository.GlobalMappingRepository;
import nl.maastro.mia.web.service.MappingService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MappingServiceApplication.class)
public class GetMappingTests {
	
    private static final String ROI_LINKERLONG = "Linkerlong";
    private static final String ROI_MILT = "Milt";
    private static final String ROI_PELVIS_BOTTEN = "Pelvis botten";
    
    private static final String RTOG_LUNG_L = "Lung_L";
    private static final String RTOG_SPLEEN = "Spleen";
    private static final String RTOG_PELVIC_BONES = "PelvicBones";
    
    @Autowired
    @Mock
    GlobalMappingRepository globalMappingRepository;
    
	@Autowired
	@InjectMocks
	MappingService mappingService;
	
	@Before
    public void setup() throws Exception {
	    GlobalMappingEntity linkerLongMapping = new GlobalMappingEntity();
	    linkerLongMapping.setRtog(RTOG_LUNG_L);
	    Mockito.when(globalMappingRepository.findOneByRoi(ROI_LINKERLONG)).thenReturn(linkerLongMapping);
	    
	    GlobalMappingEntity miltMapping = new GlobalMappingEntity();
        miltMapping.setRtog(RTOG_SPLEEN);
        Mockito.when(globalMappingRepository.findOneByRoi(ROI_MILT)).thenReturn(miltMapping);
        
        GlobalMappingEntity pelvisMapping = new GlobalMappingEntity();
        pelvisMapping.setRtog(RTOG_PELVIC_BONES);
        Mockito.when(globalMappingRepository.findOneByRoi(ROI_PELVIS_BOTTEN)).thenReturn(pelvisMapping);
    }
	
	@Test
	public void getMappingsTest(){
		List<String> roiList = new ArrayList<>();
		roiList.addAll(Arrays.asList(new String[] {ROI_LINKERLONG, ROI_MILT, ROI_PELVIS_BOTTEN, "Non-mapped roi", "", null}));
		List<String> rtogList = new ArrayList<>();
		rtogList.addAll(Arrays.asList(new String[] {RTOG_LUNG_L, RTOG_SPLEEN, RTOG_PELVIC_BONES}));
		
		Map<String, String> result = mappingService.getMappings(roiList, rtogList, "12345");
		
		assertEquals(ROI_LINKERLONG, result.get(RTOG_LUNG_L));
		assertEquals(ROI_MILT, result.get(RTOG_SPLEEN));
		assertEquals(ROI_PELVIS_BOTTEN, result.get(RTOG_PELVIC_BONES));
	}
}
