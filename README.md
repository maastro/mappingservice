# MIA Mapping Service
 
The mappingservice is a microservice developed for the MIA framework. Allows users to map RTOG names to ROI names found in RTSTRUCT files.

### About MIA ###

The Medical Image Analysis Framework (MIA) is designed for performing calculations on large volumes of DICOM data in the domain of Radiation Therapy. The framework is composed out of several [microservices](http://martinfowler.com/articles/microservices.html). Each microservice is Java application built with Spring (Boot & Cloud). **The general MIA documentation can be found [here](https://bitbucket.org/maastrosdt/mia/src/master/src/main/resources/README/README.md).**


## Prerequisites ##

- Java 8
- 512 GB RAM
- Modern browser 
- SPARQL (optional)
 

Allows users to map RTOG names to ROI names found in RTSTRUCT files.

## User manual ##

The mapping service serves 2 purposes. The first is to retrieve existing mappings made by users and a list of approved, governed mappings. The second purpose is a user interface which allows users to

1. Map RTOGs (standard structure names) to ROIs (local structure names);
2. View and delete their own stored mappings;
3. Approve suggested mappings (admin only).

To elaborate more upon these 3 functions, the user manual is split up into 3 parts: Mapping Tool, User Mappings and Mapping Approver.
 
## Mapping Tool ##

Initial view, a list of RTOGs is shown with dropdowns for ROIs and mapping options. This is where you map local structures to RTOGs
![](src/main/resources/README/img/mapt1.png)

There are 3 options for mapping:

- Map once: forget mapping after this instance
- Store: store mapping in user specific database
- Suggest: similar to store, but also suggests the mapping to be added to the RMO (for all users)
![](src/main/resources/README/img/mapt2.png)

If something went wrong, an error message is displayed:
![](src/main/resources/README/img/mapt3.png)


If the RTOG that is required is not available, you can also select the 'NotPresent' option:
![](src/main/resources/README/img/mapt4.png)


As this is a user-wide application, the parameters with which the mapping request was done, will fall out of scope once the user switches page, hence a modal is shown:
![](src/main/resources/README/img/mapt5.png)


If everything went ok, a green message appears indicating that you successfully mapped your RTOGs:
![](src/main/resources/README/img/mapt6.png)

### User Mappings ###


Here you can find your own mappings which you stored (or suggested) while mapping in the mapping tool


Overview:
![](src/main/resources/README/img/mapu1.png)


Delete certain mappings:
![](src/main/resources/README/img/mapu2.png)


After updating:
![](src/main/resources/README/img/mapu3.png)

### Mapping Approver ###

All mappings that were set to 'Suggest Mapping' in the mapping tool, end up here.


Initial view, a table with RTOG-ROI couples and a dropdown Action:
![](src/main/resources/README/img/mapa1.png)

There are 3 Actions to take per mapping:

1. Do Nothing
2. Dismiss: doesn't add the mapping to the RMO (global mappings)
3. Approve: adds the mapping to the RMO

![](src/main/resources/README/img/mapa2.png)


After selecting some options, you can press the update button to update the list of suggested mappings. If you're unsure of a mapping or if you're out of time: everything is set to 'Do Nothing' by default and will remain so.
![](src/main/resources/README/img/mapa3.png)


After pressing update:
![](src/main/resources/README/img/mapa4.png)
